import "@typechain/hardhat";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "solidity-coverage";
import "hardhat-gas-reporter";
import { HardhatUserConfig } from "hardhat/config";


const config: HardhatUserConfig = {
  defaultNetwork: "hardhat",
  solidity: {
    compilers: [{ version: "0.7.3", settings: {} }],
  },
  networks: {
    hardhat: {},
    localhost: {},
    coverage: {
      url: "http://127.0.0.1:8555", // Coverage launches its own ganache-cli client
    },
    rinkeby: {
      url: "https://eth-rinkeby.alchemyapi.io/v2/FoqLcWnaf60camthFxj4WNPh2E6-apje",
      accounts: [`0x${'9df9a83459926d1abf28376f98083e595d578ddec14215678b01f5e4b84df0ba'}`],
      gasPrice: 8000000000
    }
  }
};

export default config
